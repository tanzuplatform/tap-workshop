#!/bin/bash
set -x
set +e

export REGISTRY_USER=${HARBOR_USER:-admin}
export REGISTRY_PASSWORD=$HARBOR_PASSWORD kp secret create registry-credentials --registry ${HARBOR_LOCATION} --registry-user $REGISTRY_USER

crane auth login ${HARBOR_LOCATION} -u ${REGISTRY_USER} -p ${REGISTRY_PASSWORD}