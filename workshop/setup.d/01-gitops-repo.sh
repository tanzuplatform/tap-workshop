#!/bin/bash
set -x
set +e

export REPO_NAME=$SESSION_NAMESPACE-$(date +%s)
echo $REPO_NAME > repo.txt

git config --global user.name ${GIT_USERNAME}
git config --global user.email "${GIT_USERNAME}@example.com"

mkdir tanzu-java-web-app
cd tanzu-java-web-app
echo "# Tanzu Java Web App" >> README.MD
git init
git checkout -b tanzu-java-web-app-$SESSION_NAMESPACE
git add .
git commit -a -m "Initial Commit"

git remote add origin https://${GIT_USERNAME}:${GIT_PASSWORD}@gitlab.com/tanzuplatform/samples/tanzu-java-web-app
git push -u origin tanzu-java-web-app-$SESSION_NAMESPACE --force

cd ..
git clone https://${GIT_USERNAME}:${GIT_PASSWORD}@gitlab.com/tanzuplatform/gitops-workloads.git

mkdir -p gitops-workloads/stack/workloads/manifests/
envsubst < workload.yaml > gitops-workloads/stack/workloads/manifests/workload-$SESSION_NAMESPACE.yaml

git clone https://${GIT_USERNAME}:${GIT_PASSWORD}@gitlab.com/tanzuplatform/gitops-deliverables.git

mkdir -p gitops-workloads/stack/deliverables/manifests/
envsubst < deliverable.yaml > gitops-deliverables/stack/deliverables/manifests/deliverable-$SESSION_NAMESPACE.yaml

rm workload.yaml deliverable.yaml
