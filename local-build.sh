#!/usr/bin/env bash

set -eux -o pipefail

export KANIKO_CONTAINER=gcr.io/kaniko-project/executor:latest
export IMGPKG_REGISTRY_HOSTNAME_0=us-east4-docker.pkg.dev/pa-rvanvoorhees/packages
export BASE_IMAGE_TAG=token

docker run -v $PWD:/kaniko/pwd -v $HOME/Downloads/extensions/:/kaniko/extensions -v $HOME/Downloads/tanzu/:/kaniko/tanzu -v $HOME/.docker/config.json:/kaniko/.docker/config.json -v $PWD:$PWD $KANIKO_CONTAINER \
  --reproducible --context $PWD --dockerfile $PWD/containerfile --destination $IMGPKG_REGISTRY_HOSTNAME_0/images/eduk8s-tap-workshop:$BASE_IMAGE_TAG